#!/bin/sh
example="$(echo "$0" | sed -e 's|./test/check_\(.*\).sh|./\1|')"
tmp=''

trap 'rm -r -- "$tmp"' HUP INT ABRT EXIT

tmp="$(mktemp -d)"

if [ -z "$example" ]; then
    echo "error: empty example name" >&2
    exit 99
fi

if [ ! -f "$example" ]; then
    echo "error: example $example does not exist" >&2
    exit 99
fi

if [ ! -x "$example" ]; then
    echo "error: example $example is not executable" >&2
    exit 99
fi

rc=0

for input in ./test/inputs/*; do
    output=./test/outputs/$(basename "$input")

    if [ ! -f "$output" ]; then
        echo "error: output for input ${input} does not exist" >&2
        exit 99
    fi

    printf 'checking %s with input %s...' "$example" "$output" >&2
    "$example" < "$input" > "$tmp/output"
    
    if ! diff "$output" "$tmp/output"; then
        echo " fail: example $example for input $input" >&2
        rc=1
    else
        echo " ok." >&2
    fi
done

exit "$rc"

