#ifndef UNSHELL_TEST_SPEC_H
#define UNSHELL_TEST_SPEC_H

#include <unshell.h>

extern _Bool unshell_test_runStringSpec(char const * name, char const * input, struct unshell_Token const * expected_tokens, size_t expected_length);
extern _Bool unshell_test_runStringSpecError(char const * name, char const * input, enum unshell_Yield error);

#endif

