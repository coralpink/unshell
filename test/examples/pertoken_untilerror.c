#include <unshell.h>

int main(void) {
    enum { BUFFER_SIZE = 4096 };
    char buffer[BUFFER_SIZE] = { 0 };
    size_t length = 0;
    enum unshell_TokenType type;

    while (!unshell_nextFile(buffer, &length, BUFFER_SIZE, stdin, &type)) {
        if (type == UNSHELL_TOKEN_TEXT) {
            puts(buffer);
        }
    }

    return 0;
}

