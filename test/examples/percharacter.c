#include <unshell.h>

int main(void) {
    enum { BUFFER_SIZE = 4096 };
    char buffer[BUFFER_SIZE] = { 0 };
    size_t length = 0;
    enum unshell_State state = UNSHELL_STATE_INITIAL;

    while (length != BUFFER_SIZE) {
        int const c = fgetc(stdin);

        if (c == EOF) {
            switch (unshell_end(&state, buffer, &length)) {
                case UNSHELL_YIELDEND_TOKEN_TEXT:
                    puts(buffer);
                    break;
                case UNSHELL_YIELDEND_TOKEN_COMMENT:
                    break;
                case UNSHELL_YIELDEND_DONE:
                case UNSHELL_YIELDEND_ERROR_UNCLOSED_SINGLE_QUOTE:
                case UNSHELL_YIELDEND_ERROR_UNCLOSED_DOUBLE_QUOTE:
                    goto exit;
            }
        }
        else {
            switch (unshell_resume(&state, buffer, &length, c)) {
                case UNSHELL_YIELD1_TOKEN_TEXT:
                    puts(buffer);
                    break;
                case UNSHELL_YIELD1_TOKEN_COMMENT:
                    break;
                case UNSHELL_YIELD1_CONTINUE:
                    break;
            }
        }
    }

exit:
    return 0;
}

