#include <unshell.h>

int main(void) {
    enum { BUFFER_SIZE = 4096 };
    char buffer[BUFFER_SIZE] = { 0 };
    size_t length = 0;
    enum unshell_State state = UNSHELL_STATE_INITIAL;

    while (1) {
        switch (unshell_resumeFile(&state, buffer, &length, BUFFER_SIZE, stdin)) {
            case UNSHELL_YIELDN_TOKEN_TEXT:
                puts(buffer);
                break;
            case UNSHELL_YIELDN_TOKEN_COMMENT:
                break;
            case UNSHELL_YIELDN_CONTINUE:
                switch (unshell_end(&state, buffer, &length)) {
                    case UNSHELL_YIELDEND_TOKEN_TEXT:
                        puts(buffer);
                        break;
                    case UNSHELL_YIELDEND_TOKEN_COMMENT:
                        break;
                    case UNSHELL_YIELDEND_DONE:
                    case UNSHELL_YIELDEND_ERROR_UNCLOSED_SINGLE_QUOTE:
                    case UNSHELL_YIELDEND_ERROR_UNCLOSED_DOUBLE_QUOTE:
                        goto exit;
                }
            case UNSHELL_YIELDN_ERROR_BUFFER_FULL:
                goto exit;
        }
    }

exit:
    return 0;
}

