#include <stdio.h>
#include <unshell.h>
#define BUFFER_SIZE 512

void tokenize_file(FILE * file) {
    char value[BUFFER_SIZE] = {0};
    size_t length = 0;
    enum unshell_TokenType type;

    while (!unshell_nextFile(value, &length, BUFFER_SIZE, file, &type)) {
        if (type == UNSHELL_TOKEN_TEXT) {
            printf("%s\n", value);
        }
    }
}

int main(int argc, char * argv[]) {
    if (argc != 2) {
        return 1;
    }

    FILE * file = fopen(argv[1], "rb");

    if (file == NULL) {
        return 1;
    }

    tokenize_file(file);

    fclose(file);

    return 0;
}

