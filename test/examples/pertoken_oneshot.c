#include <unshell.h>

int main(void) {
    enum { BUFFER_SIZE = 4096 };
    char buffer[BUFFER_SIZE] = { 0 };
    size_t length = 0;
    enum unshell_State state = UNSHELL_STATE_INITIAL;

    while (1) {
        switch (unshell_readFile(&state, buffer, &length, BUFFER_SIZE, stdin)) {
            case UNSHELL_YIELD_TOKEN_TEXT:
                puts(buffer);
                break;
            case UNSHELL_YIELD_TOKEN_COMMENT:
                break;
            case UNSHELL_YIELD_ERROR_BUFFER_FULL:
            case UNSHELL_YIELD_ERROR_UNCLOSED_SINGLE_QUOTE:
            case UNSHELL_YIELD_ERROR_UNCLOSED_DOUBLE_QUOTE:
            case UNSHELL_YIELD_DONE:
                goto exit;
        }
    }

exit:
    return 0;
}

