#include <unshell.h>

int main(void) {
    struct unshell_Token * tokens;
    size_t length;
    enum unshell_Error error;

    if (!unshell_file(stdin, &tokens, &length, &error)) {
        for (size_t i = 0; i < length; ++i) {
            if (tokens[i].type == UNSHELL_TOKEN_TEXT) {
                puts(tokens[i].value);
            }
        }
    }

    unshell_freeTokens(tokens, length);

    return 0;
}

