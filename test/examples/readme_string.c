#include <stdio.h>
#include <unshell.h>
#define BUFFER_SIZE 512

void tokenize_string(char const * input) {
    char value[BUFFER_SIZE] = {0};
    size_t length = 0;
    enum unshell_TokenType type;

    while (!unshell_nextString(value, &length, BUFFER_SIZE, &input, &type)) {
        if (type == UNSHELL_TOKEN_TEXT) {
            printf("%s\n", value);
        }
    }
}

int main(int argc, char * argv[]) {
    if (argc != 2) {
        return 1;
    }

    tokenize_string(argv[1]);

    return 0;
}

