#include <assert.h>
#include <string.h>
#include "spec.h"

#define UNSHELL_TEST_MAX_TOKENS 256
#define UNSHELL_TEST_BUFFER_SIZE 256

extern _Bool unshell_test_runStringSpec(char const * name, char const * input, struct unshell_Token const * expected_tokens, size_t expected_length) {
    fprintf(stderr, "running spec %s...\n", name);

    char buffer[UNSHELL_TEST_BUFFER_SIZE] = { 0 };
    size_t length = 0;
    enum unshell_State state = UNSHELL_STATE_INITIAL;

    size_t tokens_length = 0;

    while (tokens_length <= expected_length) {
        enum unshell_Yield yield = unshell_readString(&state, buffer, &length, UNSHELL_TEST_BUFFER_SIZE, &input);

        switch (yield) {
            case UNSHELL_YIELD_TOKEN_TEXT:
            case UNSHELL_YIELD_TOKEN_COMMENT:
                (void) 0;

                struct unshell_Token expected_token = expected_tokens[tokens_length];

                if (expected_token.type != (enum unshell_TokenType) yield) {
                    fprintf(stderr, "spec %s: unexpected token type for token n = %ld (should be %d, got %d)\n", name, tokens_length, expected_token.type, yield);
                    return 1;
                }

                if (expected_token.length != length - 1) {
                    fprintf(stderr, "spec %s: unexpected token length for token n = %ld (should be %ld, got %ld)\n", name, tokens_length, expected_token.length, length - 1);
                    return 1;
                }

                if (strcmp(expected_tokens[tokens_length].value, buffer) != 0) {
                    fprintf(stderr, "spec %s: unexpected token value for token n = %ld\n\tshould be: %s\n\tgot: %s\n", name, tokens_length, expected_token.value, buffer);
                    return 1;
                }

                tokens_length += 1;
                break;

            case UNSHELL_YIELD_ERROR_BUFFER_FULL:
            case UNSHELL_YIELD_ERROR_UNCLOSED_SINGLE_QUOTE:
            case UNSHELL_YIELD_ERROR_UNCLOSED_DOUBLE_QUOTE:
                fprintf(stderr, "spec %s: unexpected error %d\n", name, yield); 
                return 1;

            case UNSHELL_YIELD_DONE:
                if (expected_length != tokens_length) {
                    fprintf(stderr, "spec %s: unexpected tokens length (should be %ld, got %ld)\n", name, expected_length, tokens_length); 
                    return 1;
                }

                return 0;
        }
    }

    return 0;
}

extern _Bool unshell_test_runStringSpecError(char const * name, char const * input, enum unshell_Yield error) {
    fprintf(stderr, "running spec %s...\n", name);

    char buffer[UNSHELL_TEST_BUFFER_SIZE] = { 0 };
    size_t length = 0;
    enum unshell_State state = UNSHELL_STATE_INITIAL;

    while (1) {
        enum unshell_Yield yield = unshell_readString(&state, buffer, &length, UNSHELL_TEST_BUFFER_SIZE, &input);

        switch (yield) {
            case UNSHELL_YIELD_TOKEN_TEXT:
            case UNSHELL_YIELD_TOKEN_COMMENT:
                break;

            case UNSHELL_YIELD_ERROR_BUFFER_FULL:
            case UNSHELL_YIELD_ERROR_UNCLOSED_SINGLE_QUOTE:
            case UNSHELL_YIELD_ERROR_UNCLOSED_DOUBLE_QUOTE:
                if (yield != error) {
                    fprintf(stderr, "spec %s: unexpected error (expected %d, got %d)\n", name, yield, error); 
                    return 1;
                }

                return 0;

            case UNSHELL_YIELD_DONE:
                fprintf(stderr, "spec %s: no error (expected %d)\n", name, error);
                return 1;
        }
    }
}

