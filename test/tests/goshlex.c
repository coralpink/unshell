#include <string.h>
#include "../spec.h"

int main(void) {
    char const input[] = "one two \"three four\" \"five \\\"six\\\"\" seven#eight # nine # ten\n eleven 'twelve\\' thirteen=13 fourteen/14";
    struct unshell_Token tokens[] = {
        { .type = UNSHELL_TOKEN_TEXT,    .value = "one" },
        { .type = UNSHELL_TOKEN_TEXT,    .value = "two" },
        { .type = UNSHELL_TOKEN_TEXT,    .value = "three four" },
        { .type = UNSHELL_TOKEN_TEXT,    .value = "five \"six\"" },
        { .type = UNSHELL_TOKEN_TEXT,    .value = "seven#eight" },
        { .type = UNSHELL_TOKEN_COMMENT, .value = " nine # ten" },
        { .type = UNSHELL_TOKEN_TEXT,    .value = "eleven" },
        { .type = UNSHELL_TOKEN_TEXT,    .value = "twelve\\" },
        { .type = UNSHELL_TOKEN_TEXT,    .value = "thirteen=13" },
        { .type = UNSHELL_TOKEN_TEXT,    .value = "fourteen/14" },
    };

    size_t const length = sizeof(tokens) / sizeof(struct unshell_Token);
    for (size_t i = 0; i < length; ++i) {
        tokens[i].length = strlen(tokens[i].value);
    }

    return unshell_test_runStringSpec("goshlex", input, tokens, length);
}

