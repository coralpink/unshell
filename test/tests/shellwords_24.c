#include <string.h>
#include "../spec.h"

int main(void) {
    char const input[] = "sh -c \"printf 'Hello\tworld\n'\"";
    struct unshell_Token tokens[] = {
        { .type = UNSHELL_TOKEN_TEXT, .value = "sh" },
        { .type = UNSHELL_TOKEN_TEXT, .value = "-c" },
        { .type = UNSHELL_TOKEN_TEXT, .value = "printf 'Hello\tworld\n'" }
    };

    size_t const length = sizeof(tokens) / sizeof(struct unshell_Token);
    for (size_t i = 0; i < length; ++i) {
        tokens[i].length = strlen(tokens[i].value);
    }

    return unshell_test_runStringSpec("shellwords_24", input, tokens, length);
}

