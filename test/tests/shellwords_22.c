#include <string.h>
#include "../spec.h"

int main(void) {
    char const input[] = "foo \\\\";
    struct unshell_Token tokens[] = {
        { .type = UNSHELL_TOKEN_TEXT, .value = "foo" },
        { .type = UNSHELL_TOKEN_TEXT, .value = "\\" }
    };

    size_t const length = sizeof(tokens) / sizeof(struct unshell_Token);
    for (size_t i = 0; i < length; ++i) {
        tokens[i].length = strlen(tokens[i].value);
    }

    return unshell_test_runStringSpec("shellwords_22", input, tokens, length);
}

