#include "../spec.h"

int main(void) {
    char const input[] = "''";
    struct unshell_Token tokens[] = {
        { .type = UNSHELL_TOKEN_TEXT, .value = "", .length = 0 },
    };

    return unshell_test_runStringSpec("shellwords_02", input, tokens, 1);
}

