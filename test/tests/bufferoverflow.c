#include <assert.h>
#include <unshell.h>

int main(void) {
    char const text[] = "1234567890";
    enum { CAPACITY = 4 };
    char buffer[CAPACITY];
    size_t length = 0;
    char const * input = text;

    enum unshell_State state = UNSHELL_STATE_INITIAL;
    enum unshell_YieldMany const yield = unshell_resumeString(&state, buffer, &length, CAPACITY, &input);

    assert(yield == UNSHELL_YIELDN_ERROR_BUFFER_FULL);
    assert(state == UNSHELL_STATE_TEXT);
    assert(length == CAPACITY);

    return 0;
}

