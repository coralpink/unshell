#include "../spec.h"

int main(void) {
    char const input[] = "hello world";
    struct unshell_Token const tokens[] = {
        { .type = UNSHELL_TOKEN_TEXT, .value = "hello", .length = 5 }, 
        { .type = UNSHELL_TOKEN_TEXT, .value = "world", .length = 5 }
    };

    return unshell_test_runStringSpec("helloworld", input, tokens, 2);
}

