#include <string.h>
#include <errno.h>
#include <config.h>
#include <unshell.h>

struct unshell_cli_Options {
    char const * program;
    char const * filename;
    _Bool help;
    _Bool all;
    _Bool flush;
    _Bool null_terminated;
};

static void unshell_cli_usage(struct unshell_cli_Options const * options);
static _Bool unshell_cli_options(struct unshell_cli_Options * options, int argc, char * argv[]);

extern int main(int argc, char* argv[]) {
    struct unshell_cli_Options options;

    if (unshell_cli_options(&options, argc, argv)) {
        return 1;
    }

    if (options.help) {
        unshell_cli_usage(&options);

        return 0;
    }

    int rc = 0;
    FILE * file = stdin;
    char const separator = options.null_terminated ? '\0' : '\n';

    if (options.filename != NULL && strcmp(options.filename, "-") != 0 && (file = fopen(options.filename, "rb")) == NULL) {
        fprintf(stderr, "%s: unable to open file %s: %s\n", options.program, options.filename, strerror(errno));
        rc = 1;
        goto exit;
    }

    size_t capacity = 65536;
    char * buffer = malloc(capacity);

    if (buffer == NULL) {
        fprintf(stderr, "%s: malloc failed: %s\n", options.program, strerror(errno));
        rc = 1;
        goto defer_close_file;
    }

    size_t length = 0;
    enum unshell_State state = UNSHELL_STATE_INITIAL;

    while (1) {
        switch (unshell_readFile(&state, buffer, &length, capacity, file)) {
            case UNSHELL_YIELD_TOKEN_TEXT:
                if (options.all) {
                    printf("%s %s%c", "TEXT", buffer, separator);
                }
                else {
                    printf("%s%c", buffer, separator);
                }

                if (options.flush) {
                    fflush(stdout);
                }
                break;

            case UNSHELL_YIELD_TOKEN_COMMENT:
                if (options.all) {
                    printf("%s %s%c", "COMMENT", buffer, separator);
                }

                if (options.flush) {
                    fflush(stdout);
                }
                break;

            case UNSHELL_YIELD_ERROR_BUFFER_FULL:
                (void) 0;
                size_t const resized_capacity = capacity * 2;

                if (resized_capacity > 1048576) {
                    fprintf(stderr, "%s: buffer size exceeding 1 MiB, bailing out\n", options.program);
                    rc = 1;
                    goto defer_free_buffer;
                }

                fprintf(stderr, "%s: resizing tokenizer buffer from %ld to %ld\n", options.program, capacity, resized_capacity);

                char * resized_buffer = realloc(buffer, resized_capacity);

                if (resized_buffer == NULL) {
                    fprintf(stderr, "%s: malloc failed: %s\n", options.program, strerror(errno));
                    rc = 1;
                    goto defer_free_buffer;
                }

                capacity = resized_capacity;
                buffer = resized_buffer;
                break;

            case UNSHELL_YIELD_ERROR_UNCLOSED_SINGLE_QUOTE:
                fprintf(stderr, "%s: unclosed single quote\n", options.program);
                rc = 1;
                goto defer_free_buffer;

            case UNSHELL_YIELD_ERROR_UNCLOSED_DOUBLE_QUOTE:
                fprintf(stderr, "%s: unclosed double quote\n", options.program);
                rc = 1;
                goto defer_free_buffer;

            case UNSHELL_YIELD_DONE:
                goto defer_free_buffer;
        }
    }

defer_free_buffer:
    free(buffer);

defer_close_file:
    if (file != stdin) {
        fclose(file);
    }

exit:
    return rc;
}

static _Bool unshell_cli_options(struct unshell_cli_Options * options, int argc, char * argv[]) {
    options->program = argv[0];
    options->filename = NULL;
    options->help = 0;
    options->all = 0;
    options->flush = 0;
    options->null_terminated = 0;
    enum { STATE_OPTION, STATE_FILE } state = STATE_OPTION;

    size_t n = 1;

    for (; n < argc; ++n) {
        char const * const current = argv[n];

        switch (state) {
            case STATE_OPTION:
                (void) 0;

                _Bool const is_flag_h = strcmp(current, "-h") == 0;
                _Bool const is_flag_help = strcmp(current, "--help") == 0;

                if (is_flag_h || is_flag_help) {
                    options->help = 1;
                    break;
                }

                _Bool const is_flag_a = strcmp(current, "-a") == 0;
                _Bool const is_flag_all = strcmp(current, "--all") == 0;

                if (is_flag_a || is_flag_all) {
                    options->all = 1;
                    break;
                }

                _Bool const is_flag_f = strcmp(current, "-f") == 0;
                _Bool const is_flag_file = strcmp(current, "--file") == 0;

                if (is_flag_f || is_flag_file) {
                    state = STATE_FILE;
                    break;
                }

                _Bool const is_flag_0 = strcmp(current, "-0") == 0;
                _Bool const is_flag_null_terminated = strcmp(current, "--null-terminated") == 0;

                if (is_flag_0 || is_flag_null_terminated) {
                    options->null_terminated = 1;
                    break;
                }

                _Bool const is_flag_flush = strcmp(current, "--flush") == 0;
                
                if (is_flag_flush) {
                    options->flush = 1;
                    break;
                }

                fprintf(stderr, "%s: unrecognized option \"%s\"\n", options->program, current);
                return 1;

            case STATE_FILE:
                options->filename = current;
                state = STATE_OPTION;
                break;
        }
    }

    if (state == STATE_FILE) {
        fprintf(stderr, "%s: -f|--file option requires a value\n", options->program);
        return 1;
    }

    return 0;
}

static void unshell_cli_usage(struct unshell_cli_Options const * options) {
    char const usage_format[] = 
"\
unshell v%s - utility for splitting stdin/files into shell-like tokens\n\
\n\
usage:\n\
    %s [-a | --all] [-f | --file file] [-0 | --null-terminated] [--flush]\n\
\n\
options\n\
    -h | --help             show usage\n\
    -a | --all              show both text and comment tokens,\n\
                            prefixes each token line with type (TEXT or COMMENT)\n\
    -f | --file file        parses given file, pass `-f -` for explicit stdin\n\
    -0 | --null-terminated  separate output tokens with '\\0' instead of '\\n'\n\
    --flush                 flush stdout on each token\n\
";

    fprintf(stderr, usage_format, VERSION, options->program);
}

