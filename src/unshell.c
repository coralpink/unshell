#include <config.h>
#include <stddef.h>
#include <string.h>
#include <unshell.h>

extern enum unshell_YieldSingle unshell_resume(enum unshell_State * state, char * buffer, size_t * length, char c) {
    switch (*state) {
        case UNSHELL_STATE_INITIAL:
            *length = 0;
            switch (c) {
                case ' ':
                case '\t':
                case '\n':
                    return UNSHELL_YIELD1_CONTINUE;
                case '"':
                    *state = UNSHELL_STATE_DOUBLE_QUOTES;
                    return UNSHELL_YIELD1_CONTINUE;
                case '\'':
                    *state = UNSHELL_STATE_SINGLE_QUOTES;
                    return UNSHELL_YIELD1_CONTINUE;
                case '\\':
                    *state = UNSHELL_STATE_ESCAPING;
                    return UNSHELL_YIELD1_CONTINUE;
                case '#':
                    *state = UNSHELL_STATE_COMMENT;
                    return UNSHELL_YIELD1_CONTINUE;
                default:
                    *state = UNSHELL_STATE_TEXT;
                    buffer[(*length)++] = c;
                    return UNSHELL_YIELD1_CONTINUE;
            }
            break;
        case UNSHELL_STATE_TEXT:
            switch (c) {
                case ' ':
                case '\t':
                case '\n':
                    *state = UNSHELL_STATE_INITIAL;
                    buffer[(*length)++] = '\0';
                    return UNSHELL_YIELD1_TOKEN_TEXT;
                case '"':
                    *state = UNSHELL_STATE_DOUBLE_QUOTES;
                    return UNSHELL_YIELD1_CONTINUE;
                case '\'':
                    *state = UNSHELL_STATE_SINGLE_QUOTES;
                    return UNSHELL_YIELD1_CONTINUE;
                case '\\':
                    *state = UNSHELL_STATE_ESCAPING;
                    return UNSHELL_YIELD1_CONTINUE;
                default:
                    buffer[(*length)++] = c;
                    return UNSHELL_YIELD1_CONTINUE;
            }
            break;
        case UNSHELL_STATE_ESCAPING:
            *state = UNSHELL_STATE_TEXT;
            buffer[(*length)++] = c;
            return UNSHELL_YIELD1_CONTINUE;
        case UNSHELL_STATE_DOUBLE_QUOTES_ESCAPING:
            switch (c) {
                case '\\':
                case '"':
                    *state = UNSHELL_STATE_DOUBLE_QUOTES;
                    buffer[*length - 1] = c;
                    return UNSHELL_YIELD1_CONTINUE;
                default:
                    *state = UNSHELL_STATE_DOUBLE_QUOTES;
                    buffer[(*length)++] = c;
                    return UNSHELL_YIELD1_CONTINUE;
            }
            break;
        case UNSHELL_STATE_DOUBLE_QUOTES:
            switch (c) {
                case '"':
                    *state = UNSHELL_STATE_TEXT;
                    return UNSHELL_YIELD1_CONTINUE;
                case '\\':
                    *state = UNSHELL_STATE_DOUBLE_QUOTES_ESCAPING;
                    buffer[(*length)++] = c;
                    return UNSHELL_YIELD1_CONTINUE;
                default:
                    buffer[(*length)++] = c;
                    return UNSHELL_YIELD1_CONTINUE;
            }
            break;
        case UNSHELL_STATE_SINGLE_QUOTES:
            switch (c) {
                case '\'':
                    *state = UNSHELL_STATE_TEXT;
                    return UNSHELL_YIELD1_CONTINUE;
                default:
                    buffer[(*length)++] = c;
                    return UNSHELL_YIELD1_CONTINUE;
            }
            break;
        case UNSHELL_STATE_COMMENT:
            switch (c) {
                case '\n':
                    *state = UNSHELL_STATE_INITIAL;
                    buffer[(*length)++] = '\0';
                    return UNSHELL_YIELD1_TOKEN_COMMENT;
                default:
                    buffer[(*length)++] = c;
                    return UNSHELL_YIELD1_CONTINUE;
            }
            break;
    }

    // unreachable
    return UNSHELL_YIELD1_CONTINUE;
}





extern enum unshell_YieldMany unshell_resumeString(enum unshell_State * state, char * buffer, size_t * length, size_t capacity, char const ** string) {
    while (1) {
        if (*length == capacity) {
            return UNSHELL_YIELDN_ERROR_BUFFER_FULL;
        }

        char const c = **string;

        if (c == '\0') {
            return UNSHELL_YIELDN_CONTINUE;
        }

        (*string)++;

        switch (unshell_resume(state, buffer, length, c)) {
            case UNSHELL_YIELD1_TOKEN_TEXT:
                return UNSHELL_YIELDN_TOKEN_TEXT;
            case UNSHELL_YIELD1_TOKEN_COMMENT:
                return UNSHELL_YIELDN_TOKEN_COMMENT;
            case UNSHELL_YIELD1_CONTINUE:
                break;
        }
    }
}

extern enum unshell_YieldMany unshell_resumeFile(enum unshell_State * state, char * buffer, size_t * length, size_t capacity, FILE * file) {
    while (1) {
        if (*length == capacity) {
            return UNSHELL_YIELDN_ERROR_BUFFER_FULL;
        }

        int const c = fgetc(file);

        if (c == EOF) {
            return UNSHELL_YIELDN_CONTINUE;
        }

        switch (unshell_resume(state, buffer, length, c)) {
            case UNSHELL_YIELD1_TOKEN_TEXT:
                return UNSHELL_YIELDN_TOKEN_TEXT;
            case UNSHELL_YIELD1_TOKEN_COMMENT:
                return UNSHELL_YIELDN_TOKEN_COMMENT;
            case UNSHELL_YIELD1_CONTINUE:
                break;
        }
    }
}

extern enum unshell_YieldEnd unshell_end(enum unshell_State * state, char * buffer, size_t * length) {
    switch (*state) {
        case UNSHELL_STATE_INITIAL:
            return UNSHELL_YIELDEND_DONE;
        case UNSHELL_STATE_ESCAPING:
        case UNSHELL_STATE_TEXT:
            *state = UNSHELL_STATE_INITIAL;
            buffer[(*length)++] = '\0';
            return UNSHELL_YIELDEND_TOKEN_TEXT;
        case UNSHELL_STATE_COMMENT:
            *state = UNSHELL_STATE_INITIAL;
            buffer[(*length)++] = '\0';
            return UNSHELL_YIELDEND_TOKEN_COMMENT;
        case UNSHELL_STATE_SINGLE_QUOTES:
            return UNSHELL_YIELDEND_ERROR_UNCLOSED_SINGLE_QUOTE;
        case UNSHELL_STATE_DOUBLE_QUOTES:
        case UNSHELL_STATE_DOUBLE_QUOTES_ESCAPING:
            return UNSHELL_YIELDEND_ERROR_UNCLOSED_DOUBLE_QUOTE;
    }

    // unreachable
    return UNSHELL_YIELDEND_DONE;
}

extern enum unshell_Yield unshell_readString(enum unshell_State * state, char * buffer, size_t * length, size_t capacity, char const ** string) {
    switch (unshell_resumeString(state, buffer, length, capacity, string)) {
        case UNSHELL_YIELDN_CONTINUE:
            return (enum unshell_Yield) unshell_end(state, buffer, length);
        case UNSHELL_YIELDN_TOKEN_TEXT:
            return UNSHELL_YIELD_TOKEN_TEXT;
        case UNSHELL_YIELDN_TOKEN_COMMENT:
            return UNSHELL_YIELD_TOKEN_COMMENT;
        case UNSHELL_YIELDN_ERROR_BUFFER_FULL:
            return UNSHELL_YIELD_ERROR_BUFFER_FULL;
    }

    // unreachable
    return UNSHELL_YIELD_DONE;
}

extern enum unshell_Yield unshell_readFile(enum unshell_State * state, char * buffer, size_t * length, size_t capacity, FILE * file) {
    switch (unshell_resumeFile(state, buffer, length, capacity, file)) {
        case UNSHELL_YIELDN_CONTINUE:
            return (enum unshell_Yield) unshell_end(state, buffer, length);
        case UNSHELL_YIELDN_TOKEN_TEXT:
            return UNSHELL_YIELD_TOKEN_TEXT;
        case UNSHELL_YIELDN_TOKEN_COMMENT:
            return UNSHELL_YIELD_TOKEN_COMMENT;
        case UNSHELL_YIELDN_ERROR_BUFFER_FULL:
            return UNSHELL_YIELD_ERROR_BUFFER_FULL;
    }

    // unreachable
    return UNSHELL_YIELD_DONE;
}

extern _Bool unshell_nextString(char * buffer, size_t * length, size_t capacity, char const ** string, enum unshell_TokenType * type) {
    enum unshell_State state = UNSHELL_STATE_INITIAL;
    switch (unshell_readString(&state, buffer, length, capacity, string)) {
        case UNSHELL_YIELD_TOKEN_TEXT:
            *type = UNSHELL_TOKEN_TEXT;
            return 0;
        case UNSHELL_YIELD_TOKEN_COMMENT:
            *type = UNSHELL_TOKEN_COMMENT;
            return 0;
        default:
            return 1;
    }
}

extern _Bool unshell_nextFile(char * buffer, size_t * length, size_t capacity, FILE * file, enum unshell_TokenType * type) {
    enum unshell_State state = UNSHELL_STATE_INITIAL;
    switch (unshell_readFile(&state, buffer, length, capacity, file)) {
        case UNSHELL_YIELD_TOKEN_TEXT:
            *type = UNSHELL_TOKEN_TEXT;
            return 0;
        case UNSHELL_YIELD_TOKEN_COMMENT:
            *type = UNSHELL_TOKEN_COMMENT;
            return 0;
        default:
            return 1;
    }
}

typedef enum unshell_Yield unshell_ReadFn(enum unshell_State * state, char * buffer, size_t * length, size_t capacity, void * user);

static _Bool unshell_custom(unshell_ReadFn * fn, void * user, struct unshell_Token ** tokens, size_t * tokens_length, enum unshell_Error * out_error) {
    _Bool rc = 0;
    enum unshell_Error error = UNSHELL_OK;

    *tokens_length = 0;
    *tokens = NULL;

    size_t tokens_capacity = UNSHELL_TOKENS_INITIAL;

    *tokens = malloc(tokens_capacity * sizeof(struct unshell_Token));

    if (*tokens == NULL) {
        error = UNSHELL_ERROR_MALLOC;
        goto exit_error;
    }

    size_t buffer_capacity = UNSHELL_BUFFER_INITIAL;
    char * buffer = malloc(buffer_capacity);
    size_t buffer_length = 0;

    if (buffer == NULL) {
        free(*tokens);
        *tokens = NULL;
        error = UNSHELL_ERROR_MALLOC;
        goto exit_error;
    }

    enum unshell_State state = UNSHELL_STATE_INITIAL;

    while (1) {
        enum unshell_Yield yield;

        switch (yield = fn(&state, buffer, &buffer_length, buffer_capacity, user)) {
            case UNSHELL_YIELD_TOKEN_TEXT:
            case UNSHELL_YIELD_TOKEN_COMMENT:
                if (*tokens_length == tokens_capacity) {
                    size_t const new_tokens_capacity = tokens_capacity * UNSHELL_BUFFER_GROWTH_MUL + UNSHELL_BUFFER_GROWTH_ADD;
                    struct unshell_Token * new_tokens = realloc(*tokens, new_tokens_capacity * sizeof(struct unshell_Token));

                    if (new_tokens == NULL) {
                        error = UNSHELL_ERROR_MALLOC;
                        goto exit_error;
                    }

                    *tokens = new_tokens;
                    tokens_capacity = new_tokens_capacity;
                }

                struct unshell_Token * token = &(*tokens)[*tokens_length];
                token->value = malloc(buffer_length);
                token->length = buffer_length - 1;

                if (token->value == NULL) {
                    error = UNSHELL_ERROR_MALLOC;
                    goto exit_error;
                }

                memcpy(token->value, buffer, buffer_length);

                if (yield == UNSHELL_YIELD_TOKEN_TEXT) {
                    token->type = UNSHELL_TOKEN_TEXT;
                }
                else {
                    token->type = UNSHELL_TOKEN_COMMENT;
                }

                (*tokens_length)++;
                buffer_length = 0;
                break;

            case UNSHELL_YIELD_ERROR_BUFFER_FULL:
                (void) 0;

                size_t const new_buffer_capacity = buffer_capacity * UNSHELL_BUFFER_GROWTH_MUL + UNSHELL_BUFFER_GROWTH_ADD;
                char * new_buffer = realloc(buffer, new_buffer_capacity);

                if (new_buffer == NULL) {
                    error = UNSHELL_ERROR_MALLOC;
                    goto exit_error;
                }

                buffer = new_buffer;
                buffer_capacity = new_buffer_capacity;
                break;

            case UNSHELL_YIELD_ERROR_UNCLOSED_SINGLE_QUOTE:
                error = UNSHELL_ERROR_UNCLOSED_SINGLE_QUOTE;
                goto exit_error;

            case UNSHELL_YIELD_ERROR_UNCLOSED_DOUBLE_QUOTE:
                error = UNSHELL_ERROR_UNCLOSED_DOUBLE_QUOTE;
                goto exit_error;

            case UNSHELL_YIELD_DONE:
                goto defer_buffer;
        }
    }

defer_buffer:
    free(buffer);

// exit:
    return rc;

exit_error:
    if (out_error != NULL) {
        *out_error = error;
    }
    rc = 1;
    goto defer_buffer;
}


extern _Bool unshell_string(char const * string, struct unshell_Token ** tokens, size_t * tokens_length, enum unshell_Error * out_error) {
    return unshell_custom((unshell_ReadFn *) &unshell_readString, &string, tokens, tokens_length, out_error);
}

extern _Bool unshell_file(FILE * file, struct unshell_Token ** tokens, size_t * tokens_length, enum unshell_Error * out_error) {
    return unshell_custom((unshell_ReadFn *) &unshell_readFile, file, tokens, tokens_length, out_error);
}

extern void unshell_freeTokens(struct unshell_Token * tokens, size_t size) {
    for (size_t i = 0; i < size; ++i) {
        free(tokens[i].value);
    }

    free(tokens);
}

