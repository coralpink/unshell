# unshell

`unshell` is a utility for splitting input into shell-like tokens. Also known as `xargs -n 1`.

## Installation

Manual: 
  ```bash
  autoreconf -i
  ./configure
  make
  make install
  ```
 
**Arch Linux** (AUR):
```
paru -S unshell
```

**Gentoo Linux**: Enable my overlay, see [codeberg.org/coralpink/gentoo](https://codeberg.org/coralpink/gentoo).
```bash
emerge --ask app-misc/unshell::coralpink
```

**Void Linux** (xbps-src): See [codeberg.org/coralpink/void](https://codeberg.org/coralpink/void) _– old version, but template can be easily adjusted_.

## Usage

Library simple usage:

```c
#include <stdio.h>
#include <unshell.h>
#define BUFFER_SIZE 512

void tokenize_string(char const * input) {
    char value[BUFFER_SIZE] = {0};
    size_t length = 0;
    enum unshell_TokenType type;

    while (!unshell_nextString(value, &length, BUFFER_SIZE, &input, &type)) {
        if (type == UNSHELL_TOKEN_TEXT) {
            printf("%s\n", value);
        }
    }
}

void tokenize_file(FILE * file) {
    char value[BUFFER_SIZE] = {0};
    size_t length = 0;
    enum unshell_TokenType type;

    while (!unshell_nextFile(value, &length, BUFFER_SIZE, file, &type)) {
        if (type == UNSHELL_TOKEN_TEXT) {
            printf("%s\n", value);
        }
    }
}
```

By the levels of abstraction and control, starting with the most abstract, the following functions are exposed:

| Function | Description |
|----------|-------------|
| `unshell_[string\|file]` | Tokenizes entire string/file in one go and allocates yielded tokens. Returns 0 when no errors where encountered and 1 otherwise. Tokens must be later freed with `unshell_freeTokens`. |
| `unshell_next[String\|File]` | Yields tokens until an error or end of string/file is encountered. Returns 0 on token yield and 1 on error. No explicit error handling. |
| `unshell_read[String\|File]` | Same as `unshell_next[String\|File]`, but caller can also handle errors, for example adjusting buffer size in case of reaching its capacity. |
| `unshell_resume[String\|File]` | Same as `unshell_read[String\|File]`, but does not treat end of string/file as end of input, this function can still be called with different strings/files, continuing tokenization. |
| `unshell_end` | Manually finialize tokenization process. Yields last token as if it was the end of the input. Will return additional errors in case of unclosed quotes. |
| `unshell_resume` | Tokenize anything by explictly inputting a single character at a time. Caller takes responsiblity for providing valid `buffer` up to `length` bytes. State must be not be modified in-between calls for proper tokenization. |
| `unshell_freeTokens` | Free tokens allocated by `unshell_[string\|file]` |

CLI read from stdin:

```console
$ echo 'first_argument second_argument "third argument (quoted)" "fourth argument - \"with nested quotes\"" # a comment' | unshell
first_argument
second_argument
third argument (quoted)
fourth argument - "with nested quotes"
$ echo 'first_argument second_argument "third argument (quoted)" "fourth argument - \"with nested quotes\"" # a comment' | unshell -a
TEXT first_argument
TEXT second_argument
TEXT third argument (quoted)
TEXT fourth argument - "with nested quotes"
COMMENT  a comment
```

CLI read from file:
```console
$ unshell -a -f /etc/fstab
COMMENT
COMMENT  See fstab(5).
COMMENT
COMMENT  <file system>	<dir>	<type>	<options>		<dump>	<pass>
TEXT tmpfs
TEXT /tmp
TEXT tmpfs
TEXT defaults,nosuid,nodev
TEXT 0
TEXT 0
```

More library examples can be seen in [src/cli.c](./src/cli.c), [test/examples/](./test/examples/).

