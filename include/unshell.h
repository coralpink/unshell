#ifndef UNSHELL_H
#define UNSHELL_H

#include <stdio.h>
#include <stdlib.h>

#define UNSHELL_TOKENS_INITIAL 256
#define UNSHELL_BUFFER_INITIAL 4096
#define UNSHELL_BUFFER_GROWTH_MUL 2
#define UNSHELL_BUFFER_GROWTH_ADD 0

enum unshell_State {
    UNSHELL_STATE_INITIAL = 1,
    UNSHELL_STATE_TEXT = 2,
    UNSHELL_STATE_ESCAPING = 3,
    UNSHELL_STATE_DOUBLE_QUOTES_ESCAPING = 4,
    UNSHELL_STATE_DOUBLE_QUOTES = 5,
    UNSHELL_STATE_SINGLE_QUOTES = 6,
    UNSHELL_STATE_COMMENT = 7
};

enum unshell_Yield {
    UNSHELL_YIELD_DONE = 1,
    UNSHELL_YIELD_TOKEN_TEXT = 2,
    UNSHELL_YIELD_TOKEN_COMMENT = 3,
    UNSHELL_YIELD_ERROR_UNCLOSED_DOUBLE_QUOTE = 4,
    UNSHELL_YIELD_ERROR_UNCLOSED_SINGLE_QUOTE = 5,
    UNSHELL_YIELD_ERROR_BUFFER_FULL = 6
};

enum unshell_YieldSingle {
    UNSHELL_YIELD1_CONTINUE      = UNSHELL_YIELD_DONE,
    UNSHELL_YIELD1_TOKEN_TEXT    = UNSHELL_YIELD_TOKEN_TEXT,
    UNSHELL_YIELD1_TOKEN_COMMENT = UNSHELL_YIELD_TOKEN_COMMENT
};

enum unshell_YieldMany {
    UNSHELL_YIELDN_CONTINUE          = UNSHELL_YIELD_DONE,
    UNSHELL_YIELDN_TOKEN_TEXT        = UNSHELL_YIELD_TOKEN_TEXT,
    UNSHELL_YIELDN_TOKEN_COMMENT     = UNSHELL_YIELD_TOKEN_COMMENT,
    UNSHELL_YIELDN_ERROR_BUFFER_FULL = UNSHELL_YIELD_ERROR_BUFFER_FULL
};

enum unshell_YieldEnd {
    UNSHELL_YIELDEND_DONE                        = UNSHELL_YIELD_DONE,
    UNSHELL_YIELDEND_TOKEN_TEXT                  = UNSHELL_YIELD_TOKEN_TEXT,
    UNSHELL_YIELDEND_TOKEN_COMMENT               = UNSHELL_YIELD_TOKEN_COMMENT,
    UNSHELL_YIELDEND_ERROR_UNCLOSED_DOUBLE_QUOTE = UNSHELL_YIELD_ERROR_UNCLOSED_DOUBLE_QUOTE,
    UNSHELL_YIELDEND_ERROR_UNCLOSED_SINGLE_QUOTE = UNSHELL_YIELD_ERROR_UNCLOSED_SINGLE_QUOTE
};

enum unshell_TokenType {
    UNSHELL_TOKEN_TEXT    = UNSHELL_YIELD_TOKEN_TEXT,
    UNSHELL_TOKEN_COMMENT = UNSHELL_YIELD_TOKEN_COMMENT
};

struct unshell_Token {
    enum unshell_TokenType type;
    char * value;
    size_t length;
};

enum unshell_Error {
    UNSHELL_OK           = 0,
    UNSHELL_ERROR_UNCLOSED_DOUBLE_QUOTE = UNSHELL_YIELD_ERROR_UNCLOSED_DOUBLE_QUOTE,
    UNSHELL_ERROR_UNCLOSED_SINGLE_QUOTE = UNSHELL_YIELD_ERROR_UNCLOSED_SINGLE_QUOTE,
    UNSHELL_ERROR_MALLOC = 8
};

extern enum unshell_YieldSingle unshell_resume(enum unshell_State * state, char * buffer, size_t * length, char c);
extern enum unshell_YieldMany unshell_resumeString(enum unshell_State * state, char * buffer, size_t * length, size_t capacity, char const ** string);
extern enum unshell_YieldMany unshell_resumeFile(enum unshell_State * state, char * buffer, size_t * length, size_t capacity, FILE * file);
extern enum unshell_YieldEnd unshell_end(enum unshell_State * state, char * buffer, size_t * length);
extern enum unshell_Yield unshell_readString(enum unshell_State * state, char * buffer, size_t * length, size_t capacity, char const ** string);
extern enum unshell_Yield unshell_readFile(enum unshell_State * state, char * buffer, size_t * length, size_t capacity, FILE * file);
extern _Bool unshell_nextString(char * buffer, size_t * length, size_t capacity, char const ** string, enum unshell_TokenType * type);
extern _Bool unshell_nextFile(char * buffer, size_t * length, size_t capacity, FILE * file, enum unshell_TokenType * type);
extern _Bool unshell_string(char const * string, struct unshell_Token ** tokens, size_t * size, enum unshell_Error * error);
extern _Bool unshell_file(FILE * file, struct unshell_Token ** tokens, size_t * size, enum unshell_Error * error);
extern void unshell_freeTokens(struct unshell_Token * tokens, size_t size);

#endif

